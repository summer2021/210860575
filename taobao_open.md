[toc]



# 1.申请应用

## 1.1  注册淘宝开放平台账号

## 1.2 认证淘宝开放平台身份信息

## 1.3 创建第三方应用





# 2.集成JustAuth

## 2.1 引入依赖

```xml
<dependency>
  <groupId>me.zhyd.oauth</groupId>
  <artifactId>JustAuth</artifactId>
  <version>${latest.version}</version>
</dependency>
```

`${latest.version}`表示当前最新的版本，可以在[这儿](https://github.com/justauth/JustAuth/releases)[ ](https://github.com/justauth/JustAuth/releases)

[ (opens new window)](https://github.com/justauth/JustAuth/releases)获取最新的版本信息。



## 2.2 创建Request

```java
AuthRequest authRequest = new AuthDouyinRequest(AuthConfig.builder()
                        .clientId("Client ID")
                        .clientSecret("Client Secret")
                        .redirectUri("http://dblog-web.zhyd.me/oauth/callback/douyin")
                        .build());
```





## 2.3 生成授权地址

我们可以直接使用以下方式生成第三方平台的授权链接：

```java
String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
```

这个链接我们可以直接后台重定向跳转，也可以返回到前端后，前端控制跳转。前端控制的好处就是，可以将第三方的授权页嵌入到iframe中，适配网站设计。



## 2.4 完整代码如下

```java
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.request.AuthWeChatOpenRequest;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping("/oauth")
public class RestAuthController {

    @RequestMapping("/render")
    public void renderAuth(HttpServletResponse response) throws IOException {
        AuthRequest authRequest = getAuthRequest();
        response.sendRedirect(authRequest.authorize(AuthStateUtils.createState()));
    }

    @RequestMapping("/callback")
    public Object login(AuthCallback callback) {
        AuthRequest authRequest = getAuthRequest();
        return authRequest.login(callback);
    }

    private AuthRequest getAuthRequest() {
        return new AuthTaobaoRequest(AuthConfig.builder()
                        .clientId("Client ID")
                        .clientSecret("Client Secret")
                        .redirectUri("http://dblog-web.zhyd.me/oauth/callback/taobao")
                        .build());
    }
}
```



# 3.授权结果





# 4.推荐

官方推荐使用 [JustAuth-demo](https://github.com/justauth/JustAuth-demo)[ ](https://github.com/justauth/JustAuth-demo)

[ (opens new window)](https://github.com/justauth/JustAuth-demo) 示例项目进行测试。

使用步骤：

- clone： https://github.com/justauth/JustAuth-demo.git[ ](https://github.com/justauth/JustAuth-demo.git)
- 将上面申请的应用信息填入到`RestAuthController#getAuthRequest`方法的对应位置中：

![](E:\水个比赛\summer2021\项目开发阶段\中期考核7.1-8.15\项目源代码\210860575\taobao_open.assets\justauth-demo-1625494545915.png)

- 启动项目，访问 http://localhost:8443[ ](http://localhost:8443)
- 选择对应的平台进行授权登录

![](E:\水个比赛\summer2021\项目开发阶段\中期考核7.1-8.15\项目源代码\210860575\taobao_open.assets\justauth-demo-page-1625494545916.png)



- 登录完成后，可以访问http://localhost:8443/users[ ](http://localhost:8443/users)[ (opens new window)](http://localhost:8443/users)查看已授权的用户

![](E:\水个比赛\summer2021\项目开发阶段\中期考核7.1-8.15\项目源代码\210860575\taobao_open.assets\justauth-demo-loginafter-1625494545916.png)



> 注意
>
> 1. 如果直接使用 JustAuth-demo 项目进行测试，那么在配置测试应用的“回调地址”时要严格按照以下格式配置：`http://localhost:8443/oauth/callback/{平台名}`
> 2. 平台名参考 `JustAuthPlatformInfo` 枚举类 `names`